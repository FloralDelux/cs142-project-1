// Buffer.h

#ifndef _Buffer_h_
#define _Buffer_h_

#include <fstream>
#include <iomanip>
#include <iostream>
#include <string>
#include <vector>
#include <sstream>

class Buffer
{
public:
    void display() const;
    const std::string & file_name() const { return file_name_; }
    void move_to_next_page();
    void move_to_previous_page();
    bool open(const std::string & file_name, bool add_to_history=true);
    bool go(const int &link);
    bool go_back();
    void set_window_height(int h) { window_height_ = h; }
    void set_line_length(int l) { line_length = l; }
    void add_history(const std::string & ln) {history.push_back(ln);}

private:
    void format_line(const std::string & line, std::vector<std::string> & vec);

    std::vector<std::string> v_lines_;
    int ix_top_line_ = 0;
    std::string file_name_;
    int window_height_;
    int line_length;

    std::vector<std::string> anchors_;
    int anchor_count_ = 0;

    std::vector<std::string> history;
};

inline void Buffer::move_to_next_page()
{
    ix_top_line_ += window_height_;
    if (ix_top_line_ >= v_lines_.size())
        ix_top_line_ -= window_height_;
}

inline void Buffer::move_to_previous_page()
{
    ix_top_line_ -= window_height_;
    if (ix_top_line_ < 0)
        ix_top_line_ = 0;
}

#endif
