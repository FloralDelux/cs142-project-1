// Buffer.cpp

#include "Buffer.h"
#include <list>

using namespace std;

void Buffer::display() const
{
    int ix_stop_line_ = ix_top_line_ + window_height_;
    for (int i = ix_top_line_; i < ix_stop_line_; ++i) {
        if (i < v_lines_.size())
            cout << std::setw(6) << i+1 << "  " << v_lines_[i];
        cout << '\n';
    }
}

void Buffer::format_line(const string & line, vector<string> & vec)
{
    string anchor_string; //string that contains anchor formatting

    //if an anchor is in the line, add it to the anchors vector
    int index_begin = line.find("<a");
    if(index_begin != string::npos) {
        //reconstruct string up to anchor and past anchor
        string new_line_begin(line, 0, index_begin);
        int index_end = line.find('>', index_begin);
        string new_line_end(line, index_end+1);

        //create string stream for anchor data
        string substring(line, index_begin+3, index_end-index_begin-3);
        stringstream a(substring);

        //push anchor location to 'anchors_' and then update 'anchor_count_'
        string temp;
        a >> temp; //store location
        anchors_.push_back(temp);
        ++anchor_count_;
        string anchor_text;
        a >> temp; //store text

        anchor_string = new_line_begin + '<' + temp + ">[" + to_string(anchor_count_) + ']' + new_line_end;

    } else {
        anchor_string = line;
    }
    //search for line breaking tags (br and p)
    stringstream ln(anchor_string);
    list<string> split_line; // break the line up into one word strings
    string temp; // hold one word/tag from line
    while(ln >> temp) {
        split_line.push_back(temp);
    }
    //search for tags
    list<string> tag_formatted_lines;
    string new_line = "";
    for(auto itr=split_line.begin(); itr!=split_line.end(); ++itr) {
        if(*itr!="<br>" && *itr!="<p>") { // no formatting
            new_line += (' ' + *itr);
        } else if (*itr=="<br>") { //line break
            tag_formatted_lines.push_back(new_line);
            new_line = "";
        } else if (*itr=="<p>") {
            tag_formatted_lines.push_back(new_line);
            tag_formatted_lines.push_back("");
            new_line = "";
        }
    }
    tag_formatted_lines.push_back(new_line);

    //split lines to prevent going wast windows width
    vector<string> words;
    string word;
    long length = 0;
    string cut_line = "";
    for(auto itr=tag_formatted_lines.begin(); itr != tag_formatted_lines.end(); ++itr) {
        words.clear(); //reset words vector
        string cut_line = ""; //reset cut_line
        length = 0; //reset length
        istringstream ss(*itr);
        while(ss >> word) {
            words.push_back(word);
        }
        for(int j = 0; j < words.size(); j++)
        {
            if(length + words[j].length() > line_length) {
                vec.push_back(cut_line);
                cut_line = words[j] + ' ';
                length = cut_line.length();
            } else {
                cut_line += words[j] + ' ';
                length+=words[j].length() + 1;
            }
        }
        vec.push_back(cut_line);
    }

}


bool Buffer::open(const string & new_file_name, bool add_to_history)
{
    std::ifstream file(new_file_name);
    if (!file)
        return false;

    if(add_to_history)
        add_history(new_file_name);

    //previous_file_=new_file_name;

    v_lines_.clear();
    anchors_.clear();
    anchor_count_ = 0;
    // Note: the vector is cleared only after we know the file
    // opened successfully.

    string line;
    while (getline(file, line))
        format_line(line, v_lines_);



    file_name_ = new_file_name;
    ix_top_line_ = 0;
    return true;
}
bool Buffer::go(const int &link)
{
    if(link<0 || link>anchor_count_) {
        return false;
    }
    else {
        if(!open(anchors_[link-1])){
            return false;
        }else{
            return true;
        }
    }
}

bool Buffer::go_back()
{
    if(history.size() <= 0){
        return false;
    }
    history.pop_back(); //remove history entry for current file

    //try to remove previous link from history
    string previous_link;
    if(history.size() > 0)
        previous_link = history[history.size()-1];
    else
        return false;

    //try to open file
    if(!open(previous_link, false))
        return false;

    return true;
}
